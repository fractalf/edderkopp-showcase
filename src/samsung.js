#!/usr/bin/env node
import minimist from 'minimist';
import { log, config, Download, Parser, WebCache } from 'edderkopp';

// Cli arguments
const argv = minimist(process.argv.slice(2), { boolean: true });
if (argv.h || argv.help || !argv._.length) {
    // console.log('Usage: ./' + __filename.split('/').pop() + ' [--download] [--log=<level>] <url>');
    // process.exit(1);
}

// Log
log.level = argv.log || 'debug';

// Url
const url = argv._[0] || 'http://www.samsung.com/no/consumer/tv-av/tv/suhd/UE55KS7005UXXE';

// Config
const conf = config.get(url);
const cookies = conf.cookies || null;

// Inject cache
Download.cache = new WebCache(__dirname + '/../www.cache');

// Force download (update cache)
if (argv.download) {
    Download.cache.remove(url);
}

// Download or get from web cache and parse html
Download.get(url, cookies)
.then(html => {
    Parser.html = html;
    const data = Parser.getData(conf.rule.product);
    // log.verbose('Length: ' + data.shops.length);
    log.verbose(data);
})
.catch(err => {
    log.error(err);
});
