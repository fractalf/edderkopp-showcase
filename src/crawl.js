#!/usr/bin/env node
import minimist from 'minimist';
import { log, config, Download, Crawler, WebCache } from 'edderkopp';

// Cli arguments
const argv = minimist(process.argv.slice(2), { boolean: true });
if (argv.h || argv.help || !argv._.length) {
    showUsage();
}

// Config
const conf = config.get(argv._[0]);
if (!conf) {
    throw new Error('No config');
}

// Options
let options = {};
if (argv.prod) {
    log.level = 'verbose';
    log.file = 'crawl.log';
    // options.save = true;
} else {
    log.level = 'debug';
    Download.cache = new WebCache(__dirname + '/../www.cache'); // inject cache
    // options.save = false;
}
if (argv.log || argv.l) { log.level = argv.log ? argv.log : argv.l; }
if (argv.nocache) { Download.cache = false; }
if (argv.download) { Download.cache.remove(url); }
// if (argv.save) { options.save = true; }

if (conf.crawl) {
    (async function() {
        try {
            let options = {
                delay: conf.crawl.delay !== undefined ? conf.crawl.delay : 8,
                maxItems: conf.crawl.maxItems !== undefined ? conf.crawl.maxItems : 8,
                maxDepth: conf.crawl.maxDepth !== undefined ? conf.crawl.maxDepth : 3
            };
            var crawler = new Crawler(conf.url, options);

            // Listen to event when data is found
            // Put data in solr, elastic, mongodb, mysql, etc
            crawler.on('handle-data', data => {
                if (typeof data === 'string') {
                    log.info('<HTML> of length ' + data.length);
                } else {
                    log.info(data)
                    // log.info(data.links.length)
                }
            });

            for (let target of conf.crawl.targets) {
                log.info('target start');

                // Handle 'get' rule(s) on how to get data
                if (typeof target.rule === 'string') {
                    if (conf.rules && conf.rules[target.rule]) {
                        target.rule = conf.rules[target.rule];
                    } else {
                        target.rule = null;
                        log.error('rule "%s" doesn\'t exist', target.rule);
                    }
                }

                // Start crawling target
                await crawler.start(target);
                log.info('target done')
            }
        } catch (err) {
            log.error(err);
        }
    })();
}

function showUsage() {
    console.log('Usage: ./' + __filename.split('/').pop() + ' [--download] [--nocache] [--log=<level>] <config file>');
    process.exit(1);
}
