module.exports =
{
    url: 'http://jokes.cc.com',
    crawl: {
        delay: 10,
        maxItems: 25,
        maxDepth: 4,
        targets: [
            {
                path: '/',
                mode: 'waterfall',
                link: [
                    '/[^/]+$',
                    '/[^/]+/[^/]+$',
                    // { elem: '#sidebar a' },
                    // { elem: '#content .pharmacy-list a' },
                ],
                rule: [
                    {
                        elem: '#content',
                        kids: [
                            {
                                name: 'joke',
                                elem: '.joke .module_content'
                            // },
                            // {
                            //     name: 'email',
                            //     elem: '*[itemprop=email]',
                            //     data: [ 'attr', 'href' ]
                            }
                        ]
                    }
                ]
            }
        ],
    }
}
