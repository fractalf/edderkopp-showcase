module.exports =
{
    url: 'https://www.apotek1.no',
    crawl: {
        delay: 10,
        maxItems: 25,
        maxDepth: 4,
        targets: [
            {
                path: '/vaare-apotek',
                mode: 'waterfall',
                link: [
                    '/vaare-apotek/[^/]+$',
                    '/vaare-apotek/[^/]+/[^/]+$',
                    // { elem: '#sidebar a' },
                    // { elem: '#content .pharmacy-list a' },
                ],
                rule: [
                    {
                        elem: '#content',
                        kids: [
                            {
                                name: 'title',
                                elem: '*[itemprop=name]'
                            },
                            {
                                name: 'email',
                                elem: '*[itemprop=email]',
                                data: [ 'attr', 'href' ]
                            }
                        ]
                    }
                ]
            }
        ],
    }
}
