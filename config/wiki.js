module.exports =
{
    url: 'https://en.wikipedia.org',
    crawl: {
        delay: 5,
        maxItems: 25,
        maxDepth: 1,
        targets: [
            {
                path: '/wiki/Crawling_(human)',
                rule: [
                    {
                        name: 'images',
                        elem: 'img',
                        data: [ 'attr', 'src' ]
                    }
                ]
            }
        ],
    }
}
