module.exports =
{
    url: 'http://www.samsung.com/no/home/',
    rule: {
        product: [
            {
                elem: '#content',
                kids: [
                    {
                        elem: '.product-info-wrap',
                        name: 'core',
                        data: 'object',
                        kids: [
                            {
                                name: 'title',
                                elem: '.product-title[itemprop=name]',
                            },
                            {
                                name: 'model',
                                elem: '.product-code[itemprop=identifier]',
                                data: [ 'attr', 'content' ]
                            },
                            {
                                name: 'imagesNormal',
                                elem: '#carousel-00 .responsive-image',
                                data: [ 'data', 'media-desktop' ]
                            },
                            {
                                name: 'imagesBig',
                                elem: '#carousel-00 .responsive-image',
                                data: [ 'data', 'img-link' ]
                            }
                        ]
                    },
                    {
                        name: 'spesifications',
                        elem: '.tech-spec-module .spec-list',
                        data: 'array',
                        kids: [
                            {
                                name: 'section',
                                elem: '.tit'
                            },
                            {
                                name: 'specs',
                                elem: '.spec',
                                data: 'array',
                                kids: [
                                    {
                                        name: 'spec',
                                        elem: '.sub-tit'
                                    },
                                    {
                                        name: 'value',
                                        elem: '.desc'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
}
