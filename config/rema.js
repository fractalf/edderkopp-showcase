module.exports =
{
    url: 'https://www.rema.no',
    rule: {
        shops: [
            {
                name: 'shops',
                elem: 'script',
                task: [
                    [ 'match', 'stores: (.*),\\n', 1 ],
                    [ 'js', 'eval(value)' ]
                ]
            }
        ]
    }
}
