module.exports =
{
    url: 'http://www.tek.no',
    crawl: {
        delay: 5,
        maxItems: 20,
        maxDepth: 2,
        targets: [
            {
                rule: [
                    {
                        name: 'links',
                        elem: 'a',
                        data: [ 'attr', 'href' ],
                        task: [ 'match', 'prisguide' ]
                    }
                ]
            },
        ]
    }
}
