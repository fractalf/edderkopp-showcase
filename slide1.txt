

Node.js in the wild
-------------------

'Edderkopp - A modular crawler/scraper written in ES6/ES7'
  by Alf Marius Foss olsen (alf@prisguide.no)

Code:
https://github.com/fractalf/edderkopp

Agenda:
 - Why?
 - Obsticles
 - Versions
 - App structure
 - Demo!

!! Please ask me about anything anytime !!


